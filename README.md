# Kohana Audit module

* Developer: A. Roots
* Created: May 2012
* Copyright: Diara OÜ

Kohana Audit is a Kohana 3.2 module for logging application events for auditing purposes.

## Install

1. Clone the repository and enable the module in `application/bootstrap.php`.
2. Execute `schema.sql` in your database
3. The User class must implement the singleton pattern - User::current() must return Auth::instance()->get_user()

## Usage example

:::php
    class Controller_Test extends Controller {
        public function action_index() {
            Audit::add(Audit::CREATE, 'Visited the test controller');
        }
    }

## Features

* Log types for different activities
* I18n support for log messages

## Dependencies

* Kohana 3.2
* Database module
* ORM module
* Auth module

Default view uses Twitter Bootstrap v2 css elements.