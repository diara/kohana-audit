<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @author Ando Roots <ando.roots@diara.ee>
 * @since 1.0
 */
abstract class Audit_Core
{

	// Release version
	const VERSION = '2.1';

	// Default number of logs to return when no limit is set
	const DEFAULT_RESULT_LIMIT = 100;

	// Log types
	const CREATE = 'CREATE'; // PUT
	const RETRIEVE = 'RETRIEVE'; // GET
	const UPDATE = 'UPDATE'; // POST
	const DELETE = 'DELETE'; // DELETE
	const ACCESS = 'ACCESS'; // View a protected/audited record
	const ADD = 'ADD'; // Add ORM relationship (ORM::add())
	const REMOVE = 'REMOVE'; // Remove ORM relationship (ORM::remove())
	const UPLOAD = 'UPLOAD'; // Upload a file
	const DOWNLOAD = 'DOWNLOAD'; // Downloaded a file
	const EMAIL = 'EMAIL'; // Sent an email

	/**
	 * Do not access directly, use the instance() static function
	 *
	 * @var Audit Stores the singleton instance
	 */
	protected static $_instance;

	/**
	 * Return View with audit rows
	 *
	 * @since 1.0
	 * @static
	 * @param Database_Result|null $rows Selection of Model_Audit_Log instances or null for last logs
	 * @return View
	 */
	public static function show(Database_Result $rows = null)
	{
		if ($rows === null) {
			$rows = ORM::factory('Audit_Log')
				->order_by('id', 'DESC')
				->limit(self::DEFAULT_RESULT_LIMIT)
				->find_all();
		}
		return View::factory('audit/table', ['rows' => $rows]);
	}

	/**
	 * Add a new audit log
	 *
	 * @since 1.0
	 * @static
	 * @param string $type Audit entry type. Can use any string, but defined constants recommended
	 * @param string $message The audit message
	 * @param array $replacements Replacement values for placeholders in the message
	 * @param array $extra_fields Assoc array for extra values, any key which exists in the audit log table
	 * @return Audit
	 */
	public static function add($type, $message, array $replacements = array(), array $extra_fields = array())
	{
		// New log
		$log = ORM::factory('Audit_Log');

		// Attributes
		$log->type = strtoupper($type);
		$log->message = $message;
		$log->replacements = json_encode($replacements);

		// Save log author
		if (class_exists('User') && method_exists('User', 'current') && is_object(User::current())) {
			$log->user_id = User::current()->pk();
		} elseif (class_exists('Auth')) {
			$user = Auth::instance()->get_user();
			if ($user instanceof Kohana_ORM) {
				$log->user_id = $user->id;
			}
		}

		// Extra fields
		if (count($extra_fields)) {
			$log->values($extra_fields, array_keys($extra_fields));
		}

		// Try saving
		try {
			$log->save();
			return self::instance();
		} catch (ORM_Validation_Exception $e) {
		} catch (Database_Exception $e) {

		}

		// Save failed
		Kohana::$log->add(
			Log::ERROR,
			'Could not save audit message ('.$e->getMessage().'). Original message: '.$message,
			$replacements
		);

		return self::instance();
	}

	/**
	 * Get the singleton instance of the class
	 *
	 * @since 1.0
	 * @static
	 * @return Audit
	 */
	final public static function instance()
	{
		if (Audit::$_instance === null) {
			Audit::$_instance = new Audit;
		}
		return Audit::$_instance;
	}

	final protected function __construct()
	{
	}

	final public function __clone()
	{
		return self::instance();
	}
}