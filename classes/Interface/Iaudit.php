<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @since 1.0
 */
interface Interface_Iaudit {

	public static function instance();

	public static function show(Database_Result $rows);

	public static function add($type, $message, array $replacements = array(), array $extra_fields = array());
}