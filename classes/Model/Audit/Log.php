<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @since 1.0
 */
class Model_Audit_Log extends ORM {

	protected $_belongs_to = array('user'=> array());

	/**
	 * Get a translated message
	 *
	 * @since 1.1
	 * @return string|bool
	 */
	public function message()
	{
		if (! $this->loaded()) {
			return FALSE;
		}

		return __($this->message, (array) json_decode($this->replacements));
	}
}