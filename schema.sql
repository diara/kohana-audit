-- Database schema for the audit module
-- Uses InnoDB to reference to users.id
--
-- Version 1.1

CREATE  TABLE IF NOT EXISTS `diara_adair`.`audit_logs` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `type` VARCHAR(32) NOT NULL COMMENT 'Log message type constant' ,
  `message` TEXT NOT NULL COMMENT 'The log message' ,
  `replacements` TEXT NULL DEFAULT NULL COMMENT 'Replacements for message placeholders' ,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `user_id` INT UNSIGNED NULL DEFAULT NULL COMMENT 'The user who created the log' ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_audit_logs_users1` (`user_id` ASC) ,
  CONSTRAINT `fk_audit_logs_users1`
    FOREIGN KEY (`user_id` )
    REFERENCES `diara_adair`.`users` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Holds system logs for audit purposes.'