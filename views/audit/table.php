<h2><?=__('Audit logs')?></h2>
<p class="help-block"><?=__('The following is a list of unfiltered audit logs, with the latest entries being displayed first. Only the top X rows are returned.')?></p>
<table class="table table-striped table-condensed table-bordered tablesorter">
	<thead>
	<tr>
		<th><?=__('ID')?></th>
		<th><?=__('Time')?></th>
		<th><?=__('Type')?></th>
		<th><?=__('User')?></th>
		<th><?=__('Message')?></th>
	</tr>
	</thead>
	<tbody>
	<?if ($rows->count()): ?>
		<? foreach ($rows as $row): ?>
		<tr>
			<td><?=$row->pk()?></td>
			<td class="span2"><?=$row->created?></td>
			<td><?=$row->type?></td>
			<td>
				<a href="<?=URL::base()?>user/<?=$row->user_id?>/edit"><?=$row->user->name?></a>
			</td>
			<td><?=$row->message?></td>
		</tr>
			<? endforeach ?>
		<? else: ?>
	<tr>
		<td colspan="5"><?=__('No rows')?></td>
	</tr>
		<?endif?>
	</tbody>
</table>